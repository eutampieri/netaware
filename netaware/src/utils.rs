use crate::providers::MACIPMappingProvider;
use crate::MacAddr;

pub fn ip_to_mac(
    ip: std::net::IpAddr,
    providers: &[Box<dyn MACIPMappingProvider>],
) -> Option<MacAddr> {
    providers
        .iter()
        .filter_map(|p| p.get_mac_address(ip))
        .next()
}

pub fn mac_to_ips(
    mac: MacAddr,
    providers: &[Box<dyn MACIPMappingProvider>],
) -> Vec<std::net::IpAddr> {
    providers
        .iter()
        .map(|p| p.get_ips(&mac))
        .fold(std::collections::HashSet::new(), |mut acc, x| {
            x.into_iter().for_each(|ip| {
                acc.insert(ip);
            });
            acc
        })
        .into_iter()
        .collect()
}
