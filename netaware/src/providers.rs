use crate::{connection::ConnectionConfiguration, errors};

pub trait Provider: std::fmt::Debug + Send + Sync {}

pub trait InternetAddressable {
    fn get_ip(&self) -> std::net::IpAddr;
}

impl<T> Provider for T where
    T: TryFrom<Option<ConnectionConfiguration>, Error = errors::Error>
        + std::fmt::Debug
        + Send
        + Sync
{
}

pub trait MACPhysicalLocationProvider: Provider + InternetAddressable {
    fn get_mac_port_map(&self) -> std::collections::HashMap<crate::MacAddr, u8>;
    fn get_mac_location(&self, mac: &crate::MacAddr) -> Option<u8> {
        self.get_mac_port_map().get(mac).copied()
    }
    fn get_port(&self, port: u8) -> Vec<crate::MacAddr> {
        self.get_mac_port_map()
            .into_iter()
            .filter(|x| x.1 == port)
            .map(|x| x.0)
            .collect()
    }
}

pub trait MACIPMappingProvider: Provider {
    fn get_ip_to_mac_map(&self) -> std::collections::HashMap<std::net::IpAddr, crate::MacAddr>;
    fn get_ips(&self, mac: &crate::MacAddr) -> Vec<std::net::IpAddr> {
        self.get_ip_to_mac_map()
            .iter()
            .filter(|(_, x)| *x == mac)
            .map(|(ip, _)| *ip)
            .collect()
    }
    fn get_mac_address(&self, ip: std::net::IpAddr) -> Option<crate::MacAddr> {
        self.get_ip_to_mac_map().get(&ip).copied()
    }
}
