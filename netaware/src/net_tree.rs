//! This module was made possible thanks to a late-night coding session with Michael Chelli.

use crate::{providers::MACPhysicalLocationProvider, MacAddr};
use serde::Serialize;
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct PhysicalLocation {
    pub id: usize,
    pub name: String,
    pub port: u8,
}

#[derive(Debug)]
pub struct Tree(pub Vec<NetTreeNode>);

#[derive(Debug)]
pub struct NetTreeNode {
    father: Option<usize>,
    children: HashMap<u8, usize>,
    pub mac_address: MacAddr,
    pub name: String,
    pub client: Box<dyn MACPhysicalLocationProvider>,
}

impl Tree {
    pub fn build(
        root: MacAddr,
        switches: Vec<(MacAddr, String, Box<dyn MACPhysicalLocationProvider>)>,
    ) -> Self {
        let mut res = switches
            .into_iter()
            .map(|(mac_address, name, client)| NetTreeNode {
                father: None,
                children: Default::default(),
                mac_address,
                name,
                client,
            })
            .collect::<Vec<_>>();

        let tables = res
            .iter()
            .map(|x| (x.mac_address, x.client.get_mac_port_map()))
            .collect::<HashMap<_, _>>();

        fn recursive_build_tree(
            father: MacAddr,
            subtree: &mut [NetTreeNode],
            subtree_start: usize,
            tables: &HashMap<MacAddr, HashMap<MacAddr, u8>>,
        ) {
            if subtree.len() <= 1 {
                return;
            }
            let root_pos = subtree
                .iter()
                .position(|x| {
                    let port_to_father = tables
                        .get(&x.mac_address)
                        .unwrap()
                        .get(&father)
                        .unwrap_or(&u8::MAX);
                    subtree
                        .iter()
                        .filter(|y| y.mac_address != x.mac_address)
                        .find(|y| {
                            // SWx port linked to SWy
                            let port = tables
                                .get(&x.mac_address)
                                .map(|z| z.get(&y.mac_address))
                                .flatten();
                            port.map(|z| z == port_to_father).unwrap_or_default()
                        })
                        .is_none()
                })
                .unwrap();
            subtree.swap(0, root_pos);
            let root_address = subtree[0].mac_address;
            let root_table = tables.get(&root_address).unwrap();

            subtree[1..].sort_by_cached_key(|x| root_table.get(&x.mac_address).unwrap_or(&u8::MAX));
            let mut boundaries = vec![1usize];
            let mut old_port: &u8 = root_table.get(&subtree[1].mac_address).unwrap_or(&0);
            for (i, x) in subtree[2..].iter().enumerate().map(|(i, x)| (i + 2, x)) {
                let current_port = root_table.get(&x.mac_address).unwrap_or(&0);
                if current_port != old_port {
                    old_port = current_port;
                    boundaries.push(i);
                }
            }
            for (i, j) in boundaries
                .iter()
                .zip(boundaries.iter().skip(1).chain(&[subtree.len()]))
            {
                recursive_build_tree(
                    root_address,
                    &mut subtree[*i..*j],
                    i + subtree_start,
                    tables,
                );
                subtree[*i].father = Some(subtree_start);
            }
        }

        recursive_build_tree(root, &mut res, 0, &tables);
        for i in 0..res.len() {
            if let Some(father_idx) = res[i].father {
                let father = &res[father_idx];
                if let Some(father_port) = tables
                    .get(&father.mac_address)
                    .map(|x| x.get(&res[i].mac_address))
                    .flatten()
                {
                    res[father_idx].children.insert(*father_port, i);
                }
            }
        }
        Tree(res)
    }
    pub fn find_path(&self, address: &MacAddr) -> Vec<PhysicalLocation> {
        let mut result = vec![];
        let (mut current_node, mut node_idx) = (&self.0[0], 0);
        loop {
            if let Some(port_for_mac) = current_node.client.get_mac_location(address) {
                result.push(PhysicalLocation {
                    id: node_idx,
                    name: current_node.name.clone(),
                    port: port_for_mac,
                });
                if let Some(new_node_idx) = current_node.children.get(&port_for_mac) {
                    current_node = &self.0[*new_node_idx];
                    node_idx = *new_node_idx;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        result
    }
    pub fn path_between(&self, source: &MacAddr, destination: &MacAddr) -> Vec<PhysicalLocation> {
        let root_to_source = self.find_path(source);
        let root_to_destination = self.find_path(destination);
        let common_path = root_to_destination
            .iter()
            .zip(root_to_source.iter())
            .take_while(|(a, b)| a.id == b.id)
            .map(|x| x.0.clone())
            .collect::<Vec<_>>();
        let lowest_common_ancestor = &common_path.iter().last();
        let common_to_source = root_to_source
            .into_iter()
            .zip(common_path.iter())
            .skip(common_path.len())
            .map(|x| x.0)
            .collect::<Vec<_>>();
        common_to_source
            .into_iter()
            .rev()
            .chain(
                lowest_common_ancestor
                    .map(|x| vec![x.clone()])
                    .unwrap_or_default()
                    .into_iter(),
            )
            .chain(
                root_to_destination
                    .into_iter()
                    .zip(common_path.iter())
                    .skip(common_path.len())
                    .map(|x| x.0),
            )
            .collect()
    }
}
