use serde::{Deserialize, Serialize};

pub enum ConnectionType {
    HTTP,
    SSH,
    SNMP,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ConnectionConfiguration {
    HTTP {
        address: std::net::SocketAddr,
        username: Option<String>,
        password: Option<String>,
    },
    SSH {
        address: std::net::SocketAddr,
        username: Option<String>,
        password: Option<String>,
    },
    SNMP {
        address: std::net::SocketAddr,
        community: String,
    },
}

impl ConnectionConfiguration {
    pub fn get_type(&self) -> ConnectionType {
        match self {
            ConnectionConfiguration::HTTP {
                address: _,
                username: _,
                password: _,
            } => ConnectionType::HTTP,
            ConnectionConfiguration::SSH {
                address: _,
                username: _,
                password: _,
            } => ConnectionType::SSH,
            ConnectionConfiguration::SNMP {
                address: _,
                community: _,
            } => ConnectionType::SNMP,
        }
    }
    pub fn get_ip(&self) -> std::net::IpAddr {
        match self {
            ConnectionConfiguration::HTTP {
                address,
                username: _,
                password: _,
            } => address.ip(),
            ConnectionConfiguration::SSH {
                address,
                username: _,
                password: _,
            } => address.ip(),
            ConnectionConfiguration::SNMP {
                address,
                community: _,
            } => address.ip(),
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
