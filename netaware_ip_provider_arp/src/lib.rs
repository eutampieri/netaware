use netaware::{connection::ConnectionConfiguration, providers::MACIPMappingProvider};

#[derive(Debug, Default)]
pub struct ARPProvider();

impl TryFrom<Option<ConnectionConfiguration>> for ARPProvider {
    type Error = netaware::errors::Error;

    fn try_from(_: Option<ConnectionConfiguration>) -> Result<Self, Self::Error> {
        Ok(Default::default())
    }
}

impl MACIPMappingProvider for ARPProvider {
    fn get_ip_to_mac_map(&self) -> std::collections::HashMap<std::net::IpAddr, netaware::MacAddr> {
        netneighbours::get_table().into_iter().collect()
    }
}
