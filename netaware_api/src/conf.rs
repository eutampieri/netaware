use serde::Deserialize;
use serde_with::{serde_as, DisplayFromStr};

#[serde_as]
#[derive(Deserialize)]
pub struct Configuration {
    pub lookup: Vec<LookupProvider>,
    pub switch: Vec<SwitchProvider>,
    #[serde_as(as = "DisplayFromStr")]
    pub root: netaware::MacAddr,
}
#[serde_as]
#[derive(Deserialize)]
pub struct SwitchProvider {
    #[serde_as(as = "DisplayFromStr")]
    pub mac: netaware::MacAddr,
    pub provider: String,
    pub name: String,
    pub configuration: netaware::connection::ConnectionConfiguration,
}

#[derive(Deserialize)]
pub struct LookupProvider {
    pub provider: String,
    pub configuration: Option<netaware::connection::ConnectionConfiguration>,
}
