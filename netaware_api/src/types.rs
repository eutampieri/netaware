use netaware::providers::MACIPMappingProvider;
use serde::Serialize;
use serde_with::{serde_as, DisplayFromStr};

#[serde_as]
#[derive(Serialize)]
pub struct Host {
    pub ips: Vec<std::net::IpAddr>,
    #[serde_as(as = "DisplayFromStr")]
    pub mac: netaware::MacAddr,
    pub path: Vec<netaware::net_tree::PhysicalLocation>,
}

pub fn host_from_mac(
    mac: netaware::MacAddr,
    tree: &netaware::net_tree::Tree,
    lookup: &[Box<dyn MACIPMappingProvider>],
) -> Option<Host> {
    let ips = netaware::utils::mac_to_ips(mac, lookup);
    let path = tree.find_path(&mac);
    if path.is_empty() && ips.is_empty() {
        None
    } else {
        Some(Host { ips, mac, path })
    }
}
