mod conf;
mod routes;
mod types;

use netaware::providers::{MACIPMappingProvider, MACPhysicalLocationProvider};
use netaware_ip_provider_arp::ARPProvider;
use netaware_ip_provider_snmp::SNMPAddressLookup;
use netaware_switch_provider_snmp::SNMPSwitch;
use once_cell::sync::OnceCell;
use poem::{get, listener::TcpListener, Result, Route, Server};

pub type Tree = std::sync::Arc<std::sync::RwLock<netaware::net_tree::Tree>>;
pub type LookupProviders = std::sync::Arc<std::sync::RwLock<Vec<Box<dyn MACIPMappingProvider>>>>;

static TREE: OnceCell<Tree> = OnceCell::new();
static LOOKUP_PROVIDERS: OnceCell<LookupProviders> = OnceCell::new();

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    let configuration: conf::Configuration = {
        let conf = std::fs::read(
            std::env::var("CONFIG_PATH").unwrap_or_else(|_| "netaware.yaml".to_string()),
        )
        .expect("Missing configuration file");
        serde_yaml::from_slice(&conf).expect("Failed to parse configuration file")
    };
    let switches = configuration
        .switch
        .iter()
        .map(|x| match x.provider.as_str() {
            "SNMPSwitch" => SNMPSwitch::try_from(Some(x.configuration.clone()))
                .ok()
                .map(Box::new)
                .map(|x| Box::<dyn MACPhysicalLocationProvider>::from(x)),
            _ => None,
        })
        .zip(configuration.switch.iter())
        .filter_map(|(s, c)| s.map(|x| (c.mac, c.name.clone(), x)))
        .collect();
    let lookup_providers: Vec<Box<dyn MACIPMappingProvider>> = configuration
        .lookup
        .iter()
        .filter_map(|x| match x.provider.as_str() {
            "ARPLookup" => ARPProvider::try_from(x.configuration.clone())
                .ok()
                .map(Box::new)
                .map(|x| Box::<dyn MACIPMappingProvider>::from(x)),
            "SNMPLookup" => SNMPAddressLookup::try_from(x.configuration.clone())
                .ok()
                .map(Box::new)
                .map(|x| Box::<dyn MACIPMappingProvider>::from(x)),
            _ => None,
        })
        .collect();
    let tree = netaware::net_tree::Tree::build(configuration.root, switches);

    let tree = std::sync::Arc::new(std::sync::RwLock::new(tree));
    let lookup_providers = std::sync::Arc::new(std::sync::RwLock::new(lookup_providers));
    TREE.set(tree).unwrap();
    LOOKUP_PROVIDERS.set(lookup_providers).unwrap();

    let app = Route::new()
        .at("/ip/:ip", get(routes::ip_info))
        .at("/mac/:mac", get(routes::mac_info))
        .at("/switch/:switch/:port", get(routes::port_info))
        .at("/switch/:switch", get(routes::switch_info))
        .at("/switches", get(routes::switches))
        .at("/path/:source/:destination", get(routes::path));
    Server::new(TcpListener::bind("0.0.0.0:3000"))
        .run(app)
        .await
}
