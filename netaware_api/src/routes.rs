use std::str::FromStr;

use poem::{
    handler,
    web::{Json, Path},
    Result,
};
use rayon::prelude::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};

use crate::{LOOKUP_PROVIDERS, TREE};

#[handler]
pub fn ip_info(Path(ip): Path<String>) -> Result<Json<Option<crate::types::Host>>> {
    let ip = std::net::IpAddr::from_str(&ip).map_err(poem::error::BadRequest)?;
    let mac = netaware::utils::ip_to_mac(ip, &*LOOKUP_PROVIDERS.get().unwrap().read().unwrap())
        .ok_or(poem::error::NotFound(std::io::Error::new(
            std::io::ErrorKind::AddrNotAvailable,
            "IP not found",
        )))?;
    Ok(Json(crate::types::host_from_mac(
        mac,
        &*TREE.get().unwrap().read().unwrap(),
        &*LOOKUP_PROVIDERS.get().unwrap().read().unwrap(),
    )))
}
#[handler]
pub fn mac_info(Path(mac): Path<String>) -> Result<Json<Option<crate::types::Host>>> {
    let mac = netaware::MacAddr::from_str(&mac).map_err(poem::error::BadRequest)?;
    Ok(Json(crate::types::host_from_mac(
        mac,
        &*TREE.get().unwrap().read().unwrap(),
        &*LOOKUP_PROVIDERS.get().unwrap().read().unwrap(),
    )))
}

#[handler]
pub fn port_info(Path((switch, port)): Path<(usize, u8)>) -> Result<Json<Vec<crate::types::Host>>> {
    let tree = &*TREE.get().unwrap().read().unwrap();
    Ok(Json(
        tree.0[switch]
            .client
            .get_port(port)
            .into_par_iter()
            .filter_map(|x| {
                crate::types::host_from_mac(
                    x,
                    &tree,
                    &*LOOKUP_PROVIDERS.get().unwrap().read().unwrap(),
                )
            })
            .collect(),
    ))
}

#[handler]
pub fn switch_info(Path(switch): Path<usize>) -> Result<Json<Vec<crate::types::Host>>> {
    let tree = &*TREE.get().unwrap().read().unwrap();
    Ok(Json(
        tree.0[switch]
            .client
            .get_mac_port_map()
            .into_par_iter()
            .filter_map(|x: (netaware::MacAddr, u8)| {
                crate::types::host_from_mac(
                    x.0,
                    &tree,
                    &*LOOKUP_PROVIDERS.get().unwrap().read().unwrap(),
                )
            })
            .filter(|x| x.path.par_iter().any(|y| y.id == switch))
            .collect(),
    ))
}

#[handler]
pub fn switches() -> Json<Vec<(usize, String)>> {
    let tree = &*TREE.get().unwrap().read().unwrap();
    Json(tree.0.iter().map(|x| x.name.clone()).enumerate().collect())
}

#[handler]
pub fn path(
    Path((source, destination)): Path<(String, String)>,
) -> Result<Json<Vec<netaware::net_tree::PhysicalLocation>>> {
    let source = netaware::MacAddr::from_str(&source).or_else(|_| {
        let ip = std::net::IpAddr::from_str(&source).map_err(poem::error::BadRequest)?;
        netaware::utils::ip_to_mac(ip, &*LOOKUP_PROVIDERS.get().unwrap().read().unwrap()).ok_or(
            poem::error::NotFound(std::io::Error::new(
                std::io::ErrorKind::AddrNotAvailable,
                "Address not found",
            )),
        )
    })?;
    let destination = netaware::MacAddr::from_str(&destination).or_else(|_| {
        let ip = std::net::IpAddr::from_str(&destination).map_err(poem::error::BadRequest)?;
        netaware::utils::ip_to_mac(ip, &*LOOKUP_PROVIDERS.get().unwrap().read().unwrap()).ok_or(
            poem::error::NotFound(std::io::Error::new(
                std::io::ErrorKind::AddrNotAvailable,
                "Address not found",
            )),
        )
    })?;
    Ok(Json(
        TREE.get()
            .unwrap()
            .read()
            .unwrap()
            .path_between(&source, &destination),
    ))
}
