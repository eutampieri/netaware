const BASE_OID: &[u32] = &[1, 3, 6, 1, 2, 1, 17, 7, 1, 2, 2, 1, 2];

use netaware::{
    connection::ConnectionConfiguration,
    errors::Error,
    providers::{InternetAddressable, MACPhysicalLocationProvider},
};

#[derive(Debug)]
pub struct SNMPSwitch {
    community: Vec<u8>,
    ip: std::net::SocketAddr,
}

impl TryFrom<Option<ConnectionConfiguration>> for SNMPSwitch {
    type Error = Error;

    fn try_from(value: Option<ConnectionConfiguration>) -> Result<Self, Self::Error> {
        match value {
            None => Err(Error::InvalidConfiguration),
            Some(value) => match value {
                ConnectionConfiguration::HTTP {
                    address: _,
                    username: _,
                    password: _,
                } => Err(Error::InvalidConfiguration),
                ConnectionConfiguration::SSH {
                    address: _,
                    username: _,
                    password: _,
                } => Err(Error::InvalidConfiguration),
                ConnectionConfiguration::SNMP { address, community } => Ok(Self {
                    community: community.as_bytes().to_vec(),
                    ip: address,
                }),
            },
        }
    }
}

impl InternetAddressable for SNMPSwitch {
    fn get_ip(&self) -> std::net::IpAddr {
        self.ip.ip()
    }
}

impl MACPhysicalLocationProvider for SNMPSwitch {
    fn get_mac_port_map(&self) -> std::collections::HashMap<netaware::MacAddr, u8> {
        let mut session = snmp::SyncSession::new(
            self.ip,
            &self.community,
            Some(std::time::Duration::from_secs(1)),
            0,
        )
        .unwrap();
        let mut oid = BASE_OID;
        let mut buf: [u32; 128] = [0; 128];
        let mut result = std::collections::HashMap::new();

        'walk: loop {
            let response = session.getnext(oid).unwrap();
            for v in response.varbinds {
                oid = (v.0).read_name(&mut buf).unwrap();
                if !oid.starts_with(BASE_OID) {
                    break 'walk;
                }
                if let snmp::Value::Integer(port) = v.1 {
                    let mut macvlan = oid.strip_prefix(BASE_OID).unwrap().iter();
                    let _vlan = macvlan.next().unwrap();
                    let mac = netaware::MacAddr::new(
                        *macvlan.next().unwrap() as u8,
                        *macvlan.next().unwrap() as u8,
                        *macvlan.next().unwrap() as u8,
                        *macvlan.next().unwrap() as u8,
                        *macvlan.next().unwrap() as u8,
                        *macvlan.next().unwrap() as u8,
                    );
                    result.insert(mac, port as u8);
                }
            }
        }
        result
    }
}
