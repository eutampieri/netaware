const BASE_OID: &[u32] = &[1, 3, 6, 1, 2, 1, 4, 22, 1, 2];
const REFRESH_INTERVAL: std::time::Duration = std::time::Duration::from_secs(60);

use std::sync::Mutex;

use netaware::{
    connection::ConnectionConfiguration, errors::Error, providers::MACIPMappingProvider,
};

type MacIpMap = std::collections::HashMap<std::net::IpAddr, netaware::MacAddr>;

#[derive(Debug)]
pub struct SNMPAddressLookup {
    community: Vec<u8>,
    ip: std::net::SocketAddr,
    cache: Mutex<Option<MacIpMap>>,
    last_update: Mutex<Option<std::time::Instant>>,
}

impl TryFrom<Option<ConnectionConfiguration>> for SNMPAddressLookup {
    type Error = Error;

    fn try_from(value: Option<ConnectionConfiguration>) -> Result<Self, Self::Error> {
        match value {
            None => Err(Error::InvalidConfiguration),
            Some(value) => match value {
                ConnectionConfiguration::HTTP {
                    address: _,
                    username: _,
                    password: _,
                } => Err(Error::InvalidConfiguration),
                ConnectionConfiguration::SSH {
                    address: _,
                    username: _,
                    password: _,
                } => Err(Error::InvalidConfiguration),
                ConnectionConfiguration::SNMP { address, community } => Ok(Self {
                    community: community.as_bytes().to_vec(),
                    ip: address,
                    cache: Default::default(),
                    last_update: Default::default(),
                }),
            },
        }
    }
}

impl SNMPAddressLookup {
    fn uncached_get_ip_to_mac_map(&self) -> MacIpMap {
        let mut session = snmp::SyncSession::new(
            self.ip,
            &self.community,
            Some(std::time::Duration::from_secs(1)),
            0,
        )
        .unwrap();
        let mut oid = BASE_OID;
        let mut buf: [u32; 128] = [0; 128];
        let mut result = std::collections::HashMap::new();

        'walk: loop {
            let response = session.getnext(oid).unwrap();
            for v in response.varbinds {
                oid = (v.0).read_name(&mut buf).unwrap();
                if !oid.starts_with(BASE_OID) {
                    break 'walk;
                }
                if let snmp::Value::OctetString(mac) = v.1 {
                    let mac =
                        netaware::MacAddr::new(mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
                    let ip = oid
                        .strip_prefix(BASE_OID)
                        .unwrap()
                        .iter()
                        .skip(1)
                        .map(|x| *x as u8)
                        .collect::<Vec<u8>>();
                    let ip = if ip.len() == 16 {
                        std::net::IpAddr::from([
                            ip[0], ip[1], ip[2], ip[3], ip[4], ip[5], ip[6], ip[7], ip[8], ip[9],
                            ip[10], ip[11], ip[12], ip[13], ip[14], ip[15],
                        ])
                    } else {
                        std::net::IpAddr::from([ip[0], ip[1], ip[2], ip[3]])
                    };
                    result.insert(ip, mac);
                }
            }
        }
        result
    }
}

impl MACIPMappingProvider for SNMPAddressLookup {
    fn get_ip_to_mac_map(&self) -> std::collections::HashMap<std::net::IpAddr, netaware::MacAddr> {
        if let (Ok(mut cache), Ok(mut last_update)) = (self.cache.lock(), self.last_update.lock()) {
            //let cache = *cache;
            //let last_update = *last_update;
            let update_needed = cache.is_none()
                || last_update
                    .map(|x| std::time::Instant::now().duration_since(x) > REFRESH_INTERVAL)
                    .unwrap_or(true);
            if update_needed {
                let map = self.uncached_get_ip_to_mac_map();
                *cache = Some(map.clone());
                *last_update = Some(std::time::Instant::now());
                map
            } else {
                cache.as_ref().unwrap().clone()
            }
        } else {
            self.uncached_get_ip_to_mac_map()
        }
    }
}
