FROM rust:latest AS builder
COPY . /
RUN ls
RUN cargo build --release
FROM debian:stable-slim
COPY --from=builder target/release/netaware_api /netaware
WORKDIR /
CMD ["/netaware"]
EXPOSE 3000
